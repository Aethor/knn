from typing import Iterable, Tuple
from random import random
from collections import Counter

def split_datas(
    data_points: Iterable[Iterable],
    labels: Iterable, ratio: float = 0.5
) -> Tuple[Iterable, Iterable, Iterable, Iterable]:
    train_label_targets = {
        label: {
            "target": occ_nb * ratio,
            "current": 0
        }
        for label, occ_nb in Counter(labels).most_common()
    }

    X_train, y_train, X_test, y_test = [], [], [], []

    for data in zip(data_points, labels):
        if train_label_targets[data[1]]["current"] < train_label_targets[data[1]]["target"]:
            X_train.append(data[0])
            y_train.append(data[1])
            train_label_targets[data[1]]["current"] += 1
        else:
            X_test.append(data[0])
            y_test.append(data[1])

    return (X_train, y_train, X_test, y_test)
    
