from typing import Tuple, Iterable, List, Callable, Dict, Any, Optional
import random, math
from statistics import median

from datapoint import DataPoint

class VantagePointTree:
    def __init__(
        self,
        data_points: Iterable[DataPoint],
        dist_fn: Callable[[Iterable, Iterable, Dict], float],
        dist_fn_kwargs: Dict = None,
        max_depth: int = 5,
        depth: int = 0
    ):

        self.dist_fn = dist_fn
        self.dist_fn_kwargs = {} if dist_fn_kwargs is None else dist_fn_kwargs

        if len(data_points) <= 1 or depth == max_depth:
            self.data_points = data_points
            self.lchild = None
            self.rchild = None
            self.vpoint = None
            self.threshold = None
            return

        self.vpoint = random.choice(data_points)
        dists = [self.dist_fn(p.vector, self.vpoint, **self.dist_fn_kwargs) for p in data_points]
        self.threshold = median(dists)

        self.lchild = VantagePointTree(
            [p for p, d in zip(data_points, dists) if d <= self.threshold],
            self.dist_fn,
            self.dist_fn_kwargs,
            max_depth=max_depth,
            depth=depth + 1
        )
        self.rchild = VantagePointTree(
            [p for p, d in zip(data_points, dists) if d > self.threshold],
            self.dist_fn,
            self.dist_fn_kwargs,
            max_depth=max_depth,
            depth=depth + 1
        )
        self.data_points = None

    def k_nearest_neighbors(self, center: Iterable, k: int = 1) -> List[Tuple[DataPoint, float]]:
        if self.is_leaf():
            return sorted(self.sort_by_dist(center, self.data_points), key=lambda p: p[0])[:k]

        dist_to_vpoint = self.dist_fn(center, self.vpoint, **self.dist_fn_kwargs)
        search_priority = ((self.lchild, self.rchild) if dist_to_vpoint <=
                           self.threshold else (self.rchild, self.lchild))

        neighbors = search_priority[0].k_nearest_neighbors(center, k)

        unsure_points_nb = 0
        for i, neighbor in enumerate(neighbors):
            if search_priority[0] == self.lchild:
                if neighbor[0] > self.threshold - dist_to_vpoint:
                    unsure_points_nb = len(neighbors) - i
                    break
            else:
                if neighbor[0] > dist_to_vpoint - self.threshold:
                    unsure_points_nb = len(neighbors) - i
                    break

        new_k = unsure_points_nb + k - len(neighbors)
        if new_k != 0:
            if search_priority[0] == self.lchild:
                return sorted(neighbors + self.rchild.k_nearest_neighbors(center, new_k))[:k]
            else:
                return sorted(neighbors + self.lchild.k_nearest_neighbors(center, new_k))[:k]

        return neighbors

    def sort_by_dist(self, center: Iterable, data_points: Iterable[DataPoint]) -> Iterable[Tuple[DataPoint, float]]:
        datas = [(self.dist_fn(p.vector, center, **self.dist_fn_kwargs), p) for p in data_points]
        return sorted(datas, key=lambda p: p[0])

    def is_leaf(self) -> bool:
        return not self.data_points is None
