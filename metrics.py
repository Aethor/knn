from typing import Iterable, Dict

class DistCounter:
    counter: int = 0

    @classmethod
    def inc_counter(self):
        DistCounter.counter += 1

    @classmethod
    def reset_counter(self):
        DistCounter.counter = 0

    @classmethod
    def print_counter(self):
        print("minkowski counter : {}".format(DistCounter.counter))

def minkowski(v1: Iterable, v2: Iterable, **kwargs: Dict) -> float:
    DistCounter.inc_counter()
    p = kwargs["p"] if "p" in kwargs else 2
    return sum([(pi - xi) ** p for pi, xi in zip(v1, v2)]) ** (1 / p)
