from typing import Iterable, Any
from math import sqrt
import random

class DataPoint:
    def __init__(self, vector: Any, label: Any):
        self.vector = vector
        self.label = label
        self.iter_index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.iter_index >= len(self.vector):
            self.iter_index = 0
            raise StopIteration
        self.iter_index += 1
        return self.vector[self.iter_index - 1]

    def __str__(self):
        return "DataPoint({}, {})".format(self.vector, self.label)

    def __repr__(self):
        return str(self)

    def __lt__(self, other) -> bool:
        return random.choice([self, other]) == self

    def __le__(self, other) -> bool:
        return random.choice([self, other]) == self