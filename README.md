---
title: Creating a K Nearest Neighbors classifier
date: "October 2019"
author: "Arthur Amalvy"
---

# Introduction

The K-nearest neighbors algorithm is one of the simplest machine learning algorithm, yet remains very effective in a number of cases. In this exercise, we attempted to build an efficient K nearest neighbours classifier. The external API is inspired from scikit-learn, a classic machine learning library, because of its ease of use. The whole project is available on my gitlab at https://gitlab.com/Aethor/knn. There is no dependency apart from pure python.


## Usage

example usage with $k = 10$ :

```python
from knn import KNNClassifier
from utils import split_datas

# assuming the function "load_datas()" load your datas in X and y
X, y = load_datas()
X_train, y_train, X_test, y_test = split_datas(X, y)
classifier = KNNClassifier()
classifier.fit(X_train, y_train)
print("score on test datas : {}".format(classifier.score(X_test, y_test, k=10)))
```

Several parameters can be fed into the classifier. For example, you can use a custom distance function instead of the standard minkowski distance, send it custom arguments, force the classifier to use a specified search method and pass it custom arguments :

```python
from knn import KNNClassifier, SearchMethod

classifier = KNNClassifier(
    dist_fn=my_dist_fn,
	dist_fn_kwargs={
	    "my_arg": True
	},
	search_method=SearchMethod.VPTREE,
	search_method_kwargs={
		"max_depth": 5
	}
)
```

You can specify any "p" you want for the provided minkowski distance function. Default is 2 (euclidian norm), but you can for example specify 1 to use Manhattan distance : 

```python
classifier = KNNClassifier(
	dist_fn_kwargs={
	    "p": 1
	}
)
```

## Example

To see an example in action, you can launch the `example.py` file using python 3 :

> python example.py

It will launch the KNN algorithm on the iris dataset and the breast cancer dataset and report accuracy, using the bruteforce method and the Vantage Point Tree method (see the **Optimizing : Vantage Point Tree** part)


## Accuracy report

Here is a report of accuracy on the iris train set :

| k   | 1 | 2 | 3  | 4  |  5 | 6  |  7 | 8  | 9  |10  | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 |
| --- |---|---|--- |--- | ---|--- | ---|--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |
|acc %|100|100|96.0|98.7|97.3|98.7|98.7|98.7|97.3|98.7|97.3|98.7|97.3|98.7|96.0|96.0|96.0|97.3|96.0|97.3|

Here is the same report for the test set :

| k   | 1  | 2  | 3  | 4  |  5 | 6  |  7 | 8  | 9  |10  | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 |
| --- |--- |--- |--- |--- | ---|--- | ---|--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |--- |
|acc %|93.3|93.3|92.0|93.3|94.6|93.3|92.0|97.3|96.0|96.0|92.0|96.0|92.0|90.7|92.0|93.3|90.7|92.0|93.3|93.3|

A few points are interesting to note :

* For the train set, accuracy for k = 1 (and k = 2, for that matter) is 100%. This is likely due to the face that, when using a single neighboor, a point is predicted using its own label (unless another point has the exact same location).
* For the test set, low values of k show overfitting, while high values indicates underfitting. The sweet spot seems to be located for k between 8 and 10 (with accuracy ranging from 96.0 to 97.3).

# Optimizing : Vantage Point Tree

The classic iris dataset, while being a good introduction dataset, does not reflect the reality of everyday data science. With only 150 entries, and 4-dimensional feature inputs, it is indeed a very small dataset. Bigger datasets can take long time to process for a KNN algorithm without carefully chosen approaches.

A common solution to reduce computing time for a lot of tasks is to use appropriates datastrucutres. the K Nearest Neighbors problem is no exception : by using a datastructure specialised in high dimensional distances queries, one can reduce the number of time the algorithm compute distances. A lot of datastructures exist for this purpose (scikit-learn, for example, uses KD-Trees and Ball-Trees), but here we chose to use a **Vantage Point Tree**.

## Principles

A Vantage Point Tree is constructed using the following method : at each level, given a set of input points, a point is randomly chosen to be the Vantage Point. Next, a threshold is chosen, such that half of the points distances to the vantage point is smaller than this threshold, and half of these distance is higher than this threshold (in short, this is the median of distances between each point and the vantage point). Then, the left node of the current level is constructed using all points inside the range created using the vantage point and the threshold, while the right node consist of all other points. The process then continues recursively until the desired depth is attained.


A search function for a VP-Tree is a recursive function that can be defined as follows :

$$f(k,a) \rightarrow \{x | x \in K\}$$

where :

* $k$ is the number of desired neighbors
* $a$ is the target point
* $K$ is the set of the k nearest neighbors to a, with $card(K) \leq k$

Searching for neighbors in a VP-Tree comes with a catch. As expected, you will start to search left if the target point distance to the current node vantage point (we denote this distance $d(a, v)$) is lower than the current threshold, and right otherwise. However, some of the returned points may not be part of the k-nearest neighbors of the target point, because points in the other node might be closer.

Let $d(a, x_i)$ be the distance from the target point $a$ to a returned point $x_i$, and $t$ the current node threshold. The set $S$ of all suspects points is composed of all $x_i$ such that: 

* $d(a, x_i) \leq t - d(a, v)$ if the search started left
* $d(a, x_i) \geq d(a, v) - t$ if the search started right

we must then search in the other node using $k = k - card(S)$ to ensure we return all nearset neighbors.


## Performance report

We tested the Vantage Point Tree on the classic "breast cancer dataset". We use a depth of 4, which corresponds to the best performance (an explanation is that the tree is more performant for the maximum depth where its leaves contain a number of points greater or equal to $k$, because it leads to less cases of searching in the two child of a node, avoiding a number of recursion calls and sorting of lists).

Here are the performance obtained in one run, for $k$ between 1 and 20 on train set and test set :

|            | time taken (s) | calls to distance function |
|    ---     |       ---      |              ---           |
| bruteforce | 13.4           |           3243300          |
| vp-tree    | 7.6            |           1670945          |

Of course, time may be a bad indication as tasks duration on a computer can be influenced by other factors, which is why we included a distance function counter. Overall, we can see we divided by approximately 2 the number of calls to the distance function, which allows us to obtain way better performance than the bruteforce method. The training cost is slightly higher, but remains perfectly acceptable (0.010s).

We also tested the tree on the simpler iris dataset (with a depth of 2), and we found that even with this small and less dimensional dataset, the tree performed slightly better :

|            | time taken (s) | calls to distance function |
|    ---     |       ---      |              ---           |
| bruteforce | 0.38           |           225000           |
| vp-tree    | 0.34           |           169742           |

Please note the accuracy can vary between the tree and bruteforce methods, because point storage can change results when several points have the same coordinates.
