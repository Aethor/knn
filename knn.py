from typing import Iterable, Callable, Dict, Any, Optional, List
from collections import Counter
from math import sqrt
from copy import deepcopy
from enum import Enum

from metrics import minkowski
from trees import VantagePointTree
from datapoint import DataPoint

class SearchMethod(Enum):
    BRUTEFORCE = 0
    VPTREE = 1


class KNNClassifier:
    def __init__(
        self,
        dist_fn: Callable[[Iterable, Iterable, Dict], float] = minkowski,
        dist_fn_kwargs: Dict = None,
        search_method: Enum = SearchMethod.VPTREE,
        search_method_kwargs: Dict = None
    ):
        """ KKN Classifier initialisation method

            :param dist_fn: custom distance function (default : minkowski)
            :param dist_fn_kwargs: arguments to pass to the distance function
            :param search_method: one of SearchMethod.BRUTEFORCE 
                or SearchMethod.VPTREE (default to vp-tree)
            :param search_method_kwargs: argument for the chosen search algorithm
        """

        self.search_method = search_method
        if search_method == SearchMethod.BRUTEFORCE:
            self.data_points: Optional[Iterable[DataPoint]] = None
        elif search_method == SearchMethod.VPTREE:
            self.vptree: Optional[VantagePointTree] = None
        else:
            raise Exception("unknown search method : {}".format(search_method))
        self.search_method_kwargs = search_method_kwargs if not search_method_kwargs is None else {}

        self.dist_fn = dist_fn
        self.dist_fn_kwargs = dist_fn_kwargs if not dist_fn_kwargs is None else {}

    def fit(self, data_points: Iterable[Iterable], labels: Iterable[Iterable]):

        if self.search_method == SearchMethod.BRUTEFORCE:
            self.data_points = [DataPoint(p, l) for p, l in zip(data_points, labels)]

        elif self.search_method == SearchMethod.VPTREE:
            self.vptree = VantagePointTree(
                [DataPoint(p, l) for p, l in zip(data_points, labels)],
                dist_fn=self.dist_fn,
                dist_fn_kwargs=self.dist_fn_kwargs,
                **self.search_method_kwargs
            )

    def predict(self, data_points: Iterable[Iterable], k: int = 2) -> List[Any]:
        predictions = []
        for data_point in data_points:
            neighbours_labels = [n.label for n in self.neighbors(data_point, k)]
            predictions.append(Counter(neighbours_labels).most_common()[0][0])
        return predictions

    def score(self, data_points: Iterable[Iterable], labels: Iterable, k: int = 2) -> float:
        if len(data_points) == 0:
            return 0
        predictions = self.predict(data_points, k)
        return sum([1 if z[0] == z[1] else 0 for z in zip(predictions, labels)]) / len(data_points)

    def neighbors(self, center: Iterable, k: int = 2) -> List[DataPoint]:
        if self.search_method == SearchMethod.BRUTEFORCE:
            sorted_datas = sorted(self.data_points, key=lambda p: self.dist_fn(
                p.vector, center, **self.dist_fn_kwargs))
            return list(sorted_datas)[:k]
        elif self.search_method == SearchMethod.VPTREE:
            neighbors_datas = self.vptree.k_nearest_neighbors(center, k)
            return [d[1] for d in neighbors_datas]
