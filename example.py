from time import time
from typing import Callable, Iterable
import cProfile

from knn import KNNClassifier, SearchMethod
from utils import split_datas
from metrics import DistCounter

def time_log(fn: Callable) -> Callable:
    def wrapper(*args, **kwargs):
        before = time()
        fn(*args, **kwargs)
        after = time()
        print("execution took : {}".format(after - before))
    return wrapper

def test_search_method(
    X_train: Iterable,
    y_train: Iterable,
    X_test: Iterable,
    y_test: Iterable,
    search_method: SearchMethod
):
    print("training classifier...")
    classifier = KNNClassifier(
        search_method=search_method,
        search_method_kwargs= {
            "max_depth": 4
        }
    )
    classifier.fit = time_log(classifier.fit)
    classifier.fit(X_train, y_train)

    DistCounter.reset_counter()
    before = time()
    print("classifying...")
    for k in range(1, 21):
        print("score on train data with k = " + str(k) + " : " +
            str(classifier.score(X_train, y_train, k=k) * 100) + "%")
    for k in range(1, 21):
        print("score on test data with k = " + str(k) + " : " +
            str(classifier.score(X_test, y_test, k=k) * 100) + "%")
    after = time()
    DistCounter.print_counter()
    print("classification took {}".format(after - before), end="\n\n")


print("loading iris datas...")
lines = None
with open("iris.data") as f:
    lines = f.read().split("\n")

X = [[float(d) for d in line.split(",")[0:4]] for line in lines]
y = [line.split(",")[-1] for line in lines]

print("splitting data into train and test sets...")
X_train, y_train, X_test, y_test = split_datas(X, y)

print("using brute force")
test_search_method(X_train, y_train, X_test, y_test, SearchMethod.BRUTEFORCE)

print("using Vantage Point Tree")
test_search_method(X_train, y_train, X_test, y_test, SearchMethod.VPTREE)


print("loading breast cancer datas...")
lines = None
with open("wdbc.data") as f:
    lines = f.read().split("\n")

X = [[float(d) for d in line.split(",")[2:]] for line in lines]
y = [line.split(",")[1] for line in lines]

print("splitting data into train and test sets...")
X_train, y_train, X_test, y_test = split_datas(X, y)

print("using brute force")
test_search_method(X_train, y_train, X_test, y_test, SearchMethod.BRUTEFORCE)

print("using Vantage Point Tree")
test_search_method(X_train, y_train, X_test, y_test, SearchMethod.VPTREE)
